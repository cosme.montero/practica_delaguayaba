class PokemonTrainersController < ApplicationController
    def index
        @trainers = PokemonTrainer.all
    end

    def show
        @trainers = PokemonTrainer.find(params[:id]) 
    end

    def new
        @trainers = PokemonTrainer.new 
    end

    def create
        @trainers = PokemonTrainer.create(trainer_params)
        if @trainers.save
            pokemon_to_trainer(params[:pokemon_trainer][:captured_pokemon], params[:id])
            redirect_to pokemon_trainers_path
        else
            render :new
        end 
    end

    def edit
        @trainers = PokemonTrainer.find(params[:id])
    end

    def update
        @trainers = PokemonTrainer.find(params[:id])
            if @trainers.update(trainer_params)
                pokemon_to_trainer(params[:pokemon_trainer][:captured_pokemon], params[:id])
                redirect_to pokemon_trainers_path
            else
                render :edit
            end
    end

    def destroy
        @trainers = PokemonTrainer.find(params[:id])
        @trainers.destroy
        redirect_to pokemon_trainers_path
    end

    private
        def pokemon_to_trainer(pokemon_id, trainer_id)
            @pokemon = CapturedPokemon.find(pokemon_id)
            @pokemon.update(:pokemon_trainer_id => trainer_id)
        end
        

    private
        def trainer_params
            params.require(:pokemon_trainer).permit(:name, :last_name, :region, :email)
        end
end
