class PokemonsController < ApplicationController
  before_action :set_pokemons, only: :index

  def index
    
  end

  def show
    @pokemons = CapturedPokemon.all
  end
  
  def new
    @pokemons = PokemonService.new
    create()
  end

  def create
    @pokemon = CapturedPokemon.create(get_pokemon)
    puts @pokemon.to_json    
    @pokemon.save
    redirect_to pokemons_path
  end  

  def update
    @pokemon = CapturedPokemon.find(params[:id])
    @pokemon.update()
    redirect_to pokemon_trainers_path
  end

  private
    def get_pokemon
      @pokemons = PokemonService.new().get_one(params[:url])
    end

  private
    def set_pokemons
      @pokemons = PokemonService.new().get_all
    end
end
