module PokemonTrainersHelper
    def pokemons_for_select
        CapturedPokemon.all.pluck(:name, :id)
    end
end
