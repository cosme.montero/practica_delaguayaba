class CapturedPokemon < ApplicationRecord
    belongs_to :pokemon_trainer, optional: true
end
