class PokemonTrainer < ApplicationRecord
    has_many :captured_pokemon    
    validates_presence_of :name, :last_name
    validates_format_of :email, with: URI::MailTo::EMAIL_REGEXP
end
