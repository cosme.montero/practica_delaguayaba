class PokemonService

  def get_all
    HTTParty.get('https://pokeapi.co/api/v2/pokemon/?limit=100')['results']
  end

  def get_one (url)
    @json = HTTParty.get(url)
    @resp = JSON.parse(@json.body)
    @pokemon = build_pokemon(@resp)
    return @pokemon
  end

  private
    def build_pokemon(resp)
      @slots = Array.new
      resp['types'].each_with_index do |types, i|
        @slots[i] = types["type"]["name"]
      end
      @pokemon = {
        "api_version" => "V2",
        "name" => resp["name"].to_s,
        "first_slot" => @slots[0].to_s,  
        "second_slot" => @slots[1].to_s,      
        "img_url" => resp["sprites"]["other"]["official-artwork"]["front_default"].to_s
      }
    end
end
