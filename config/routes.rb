Rails.application.routes.draw do
  resources :pokemon_trainers
  resources :pokemons
  
  root "pokemons#index"
end
