class CreatePokemonTrainers < ActiveRecord::Migration[5.2]
  def change
    create_table :pokemon_trainers do |t|
      t.string :name, :null => false
      t.string :last_name, :null => false
      t.string :region
      t.string :email
    end
  end
end
