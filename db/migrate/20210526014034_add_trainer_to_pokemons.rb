class AddTrainerToPokemons < ActiveRecord::Migration[5.2]
  def change
    add_reference :captured_pokemons, :pokemon_trainer, foreign_key: true
  end
end
