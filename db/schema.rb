# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_26_014034) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "captured_pokemons", force: :cascade do |t|
    t.string "api_version"
    t.string "name"
    t.string "first_slot"
    t.string "second_slot"
    t.string "img_url"
    t.bigint "pokemon_trainer_id"
    t.index ["pokemon_trainer_id"], name: "index_captured_pokemons_on_pokemon_trainer_id"
  end

  create_table "pokemon_trainers", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.string "region"
    t.string "email"
  end

  add_foreign_key "captured_pokemons", "pokemon_trainers"
end
